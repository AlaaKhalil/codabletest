//
//  ViewController.swift
//  CodableTest
//
//  Created by mahmoud farid on 11/11/18.
//  Copyright © 2018 mahmoud farid. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let urlString = "https://api.letsbuildthatapp.com/jsondecodable/course"
        guard let url = URL(string: urlString) else
        {return}
        URLSession.shared.dataTask(with: url) { (data, response, err) in
            let dataDecoder = JSONDecoder()
            do{
                let result = try dataDecoder.decode(Result.self, from: data!)
                print(result.id)
                print(result.name)
                print(result.link)
            }
            catch{
                print(err)
            }
            
        }.resume()
    }


}
class Result: Codable{
    let id : Int
    let name : String
    let link: String
    let imageUrl : String
    let number_of_lessons : Int
    
    init(id: Int, name:String, link:String, imageUrl: String, number_of_lessons: Int) {
        self.id = id
        self.name = name
        self.link = link
        self.imageUrl = imageUrl
        self.number_of_lessons = number_of_lessons
       
    }
}

